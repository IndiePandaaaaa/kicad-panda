# KiCAD-Panda

## Description

This repository is a collection of different footprints and symbols created for KiCAD.

## Installation

- clone the repository to your KiCAD library, e.g. for linux:

    git clone https://gitlab.com/IndiePandaaaaa/kicad-panda.git ~/.local/share/kicad/libraries/

- import the library with KiCAD → Preferences → Manage Symbol Libraries... → add the custom library

